package com.globalia.configuration;

import com.globalia.enumeration.LogType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@NoArgsConstructor
@ToString
public class TokenConfig implements Serializable {

	private static final long serialVersionUID = 2200232972661713005L;

	private String secret;
	private String secretEncrypt;
	private String encryptAlgorithm;
	private int expirationAdminInMs;
	private int expirationInMs;
	private String forcedUrl;
	private LogType allowedLogLevel;
}
