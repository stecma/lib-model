package com.globalia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.exception.Error;
import com.globalia.exception.Warning;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class BaseItem extends Item {

	private static final long serialVersionUID = 7164774002225965046L;

	@JsonProperty("act")
	private boolean isActive;
	@JsonProperty("del")
	private boolean deleted;
	@JsonProperty("codes")
	private Set<PairValue> codes;
	@JsonProperty("warnings")
	@ToString.Exclude
	private Set<Warning> warnings;
	@JsonProperty("error")
	@ToString.Exclude
	private Error error;
}
