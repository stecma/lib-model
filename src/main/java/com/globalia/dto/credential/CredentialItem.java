package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.LinkedHashSet;
import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class CredentialItem extends BaseItemCredential {

	private static final long serialVersionUID = 5956428205348585362L;

	@JsonProperty("alias")
	private String alias;
	@JsonProperty("desc")
	private String description;
	@JsonProperty("status")
	private String status;
	@JsonProperty("brand")
	private MasterItem brand;
	@JsonProperty("srvtype")
	private MasterItem serviceType;
	@JsonProperty("slc")
	private MasterItem saleChannel;
	@JsonProperty("mdt")
	private MasterItem distributionModel;
	@JsonProperty("test")
	private boolean test;
	@JsonProperty("demo")
	private boolean demo;
	@JsonProperty("xsell")
	private boolean xsell;
	@JsonProperty("tagnac")
	private boolean tagNationality;
	@JsonProperty("market")
	private MasterItem market;
	@JsonProperty("depth")
	private MasterItem depth;
	@JsonProperty("salesModel")
	private MasterItem salesModel;
	@JsonProperty("hope")
	private boolean hope;
	@JsonProperty("user")
	private MasterItem user;
	@JsonProperty("lang")
	private MasterItem language;
	@JsonProperty("cur")
	private MasterItem currency;
	@JsonProperty("platforms")
	private Set<MasterItem> platforms;
	@JsonProperty("hotelX")
	private boolean hotelX;
	@JsonProperty("rates")
	private Set<MasterItem> rates;
	@JsonProperty("availcost")
	private boolean availCost;
	@JsonProperty("priceChange")
	private boolean priceChange;
	@JsonProperty("products")
	private Set<MasterItem> products;
	@JsonProperty("env")
	private MasterItem environment;
	@JsonProperty("request")
	private MasterItem request;
	@JsonProperty("share")
	private Integer share;
	@JsonProperty("keyMap")
	private LinkedHashSet<KeyMap> keyMap;
	@JsonProperty("filter")
	private CredentialFilter filter;
	@JsonProperty("groups")
	private Set<GroupCredentialItem> groups;
	@JsonProperty("trades")
	private Set<TradePolicyItem> trades;
	@JsonProperty("clients")
	private Set<CustomerItem> clients;
	@JsonProperty("releases")
	private Set<ReleaseItem> releases;
	@JsonProperty("cancelCosts")
	private Set<CancelCostItem> cancelCosts;
	@JsonProperty("currencies")
	private Set<CurrencyItem> currencies;
	@JsonProperty("numClients")
	private MasterItem numClients;
	@JsonProperty("note")
	private LinkedHashSet<Note> notes;
	@JsonProperty("systemId")
	private String systemId;
	@JsonProperty("aliasInDB")
	private String aliasInDB;
	@JsonProperty("systemIdInDB")
	private String systemIdInDB;
}
