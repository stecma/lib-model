package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.Parameter;
import com.globalia.enumeration.credential.MatchLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class CurrencyItem extends BaseItemCredential {

	private static final long serialVersionUID = 4837609669946689507L;

	@JsonProperty("credential")
	private String credential;
	@JsonProperty("assignment")
	private Assignment assignment;
	@JsonProperty("status")
	private MatchLevel status;
	@JsonProperty("params")
	private Set<Parameter> params;
}
