package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.DateRange;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ExternalFilter implements Serializable {

	private static final long serialVersionUID = 479035670345615342L;

	@JsonProperty("corp")
	private String corp;
	@JsonProperty("user")
	private String user;
	@JsonProperty("factType")
	private String factType;
	@JsonProperty("connector")
	private String connector;
	@JsonProperty("checkIn")
	private DateRange checkIn;
	@JsonProperty("book")
	private DateRange book;
}