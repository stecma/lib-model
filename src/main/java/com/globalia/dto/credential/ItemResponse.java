package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.globalia.dto.BaseItemResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode(callSuper = false)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
public class ItemResponse extends BaseItemResponse {

	private static final long serialVersionUID = -7644504430574868916L;

	private CredentialItem credential;
	private CustomerItem customer;
	private CancelCostItem cancelCost;
	private ExternalCredentialItem external;
	private GroupCredentialItem group;
	private ReleaseItem release;
	private SupplierItem supplier;
	private SupplierByZoneItem supplierByZone;
	private TradePolicyItem tradePolicy;
	private MatchItem match;
	private MassiveItem massive;
	private CurrencyItem currency;
	private MasterItem master;
	private CredentialCounterItem credentialIndexCounter;
	private CredentialCounterItem credentialAliasCounter;
}
