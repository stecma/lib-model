package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.enumeration.credential.InfoStatic;
import com.globalia.enumeration.credential.SupplierStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.LinkedHashSet;
import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class SupplierItem extends BaseItemCredential {

	private static final long serialVersionUID = -927574307004248059L;

	@JsonProperty("tse")
	private MasterItem serviceType;
	@JsonProperty("sys")
	private MasterItem system;
	@JsonProperty("mce")
	private MasterItem buyModel;
	@JsonProperty("mlu")
	private MasterItem user;
	@JsonProperty("cpc")
	private MasterItem clientPref;
	@JsonProperty("clients")
	private Set<MasterItem> clients;
	@JsonProperty("tcd")
	private Set<MasterItem> topDestinations;
	@JsonProperty("exts")
	private Set<GroupCredentialItem> externalSys;
	@JsonProperty("status")
	private SupplierStatus status;
	@JsonProperty("xsell")
	private boolean xsell;
	@JsonProperty("giata")
	private String giata;
	@JsonProperty("mhr")
	private boolean multiHotel;
	@JsonProperty("timeout")
	private boolean timeout;
	@JsonProperty("test")
	private boolean test;
	@JsonProperty("stcinf")
	private InfoStatic staticInfo;
	@JsonProperty("block")
	private boolean block;
	@JsonProperty("availgts")
	private boolean availCost;
	@JsonProperty("addrem")
	private boolean addRemarks;
	@JsonProperty("sendrem")
	private boolean sendRemarks;
	@JsonProperty("vcc")
	private boolean vcc;
	@JsonProperty("shwrate")
	private boolean showRates;
	@JsonProperty("numpas")
	private String numpas;
	@JsonProperty("disact")
	private boolean discrepancyStatus;
	@JsonProperty("disext")
	private boolean discrepancyExtended;
	@JsonProperty("effcost")
	private boolean effectiveCost;
	@JsonProperty("regimen")
	private boolean generalRegimen;
	@JsonProperty("roomMax")
	private int roomMax;
	@JsonProperty("hotelNum")
	private int hotelNum;
	@JsonProperty("threadMax")
	private int threadMax;
	@JsonProperty("sla")
	private boolean hasSla;
	@JsonProperty("keyMap")
	private LinkedHashSet<KeyMap> keyMap;
	@JsonProperty("supByZone")
	private boolean supByZone;
	@JsonProperty("filter")
	private SupplierFilter filter;
	@JsonProperty("note")
	private LinkedHashSet<Note> notes;
	@JsonProperty("gennote")
	private LinkedHashSet<Note> genNotes;
}

