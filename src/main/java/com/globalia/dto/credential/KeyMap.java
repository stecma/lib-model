package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
public class KeyMap implements Serializable {

	private static final long serialVersionUID = 8489938999533830938L;

	@JsonProperty("field")
	private String field;
	@JsonProperty("text")
	private String text;
	@JsonProperty("parent")
	private String parent;
	@JsonProperty("order")
	private int order;
}
