package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class CredentialFilter implements Serializable {

	private static final long serialVersionUID = 4608771733410640699L;

	@JsonProperty("brand")
	private String brand;
	@JsonProperty("tse")
	private String serviceType;
	@JsonProperty("depth")
	private String depth;
	@JsonProperty("hotx")
	private String hotelX;
	@JsonProperty("acv")
	private String availCost;
	@JsonProperty("pcg")
	private String priceChange;
	@JsonProperty("test")
	private String test;
	@JsonProperty("slc")
	private String saleChannel;
	@JsonProperty("mdt")
	private String distributionModel;
	@JsonProperty("salesModel")
	private String salesModel;
	@JsonProperty("lang")
	private String language;
	@JsonProperty("env")
	private String environment;
	@JsonProperty("req")
	private String request;
	@JsonProperty("plat")
	private String platform;
	@JsonProperty("rate")
	private String rate;
	@JsonProperty("prd")
	private String productOrigin;
	@JsonProperty("customer")
	private String customer;
	@JsonProperty("agroup")
	private String agencyGroup;
}