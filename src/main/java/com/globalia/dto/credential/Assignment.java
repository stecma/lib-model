package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.DateRange;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.LinkedHashSet;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Assignment implements Serializable {

	private static final long serialVersionUID = 6306413432813627049L;

	@JsonProperty("checkIn")
	private DateRange checkIn;
	@JsonProperty("book")
	private DateRange book;
	@JsonProperty("note")
	private LinkedHashSet<Note> notes;
	@JsonProperty("gennote")
	private LinkedHashSet<Note> genNotes;
}
