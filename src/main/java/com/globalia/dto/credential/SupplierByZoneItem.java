package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.exception.Error;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode()
public class SupplierByZoneItem implements Serializable {

	private static final long serialVersionUID = -8384632577301922357L;

	@JsonProperty("id")
	private String id;
	@JsonProperty("coduser")
	private String coduser;
	@JsonProperty("codsis")
	private String codsis;
	@JsonProperty("codpro")
	private String codpro;
	@JsonProperty("refzge")
	private String refzge;
	@JsonProperty("codpai")
	private String codpai;
	@JsonProperty("codepr")
	private String codepr;
	@JsonProperty("codare")
	private String codare;
	@JsonProperty("subprv")
	private String subprv;
	@JsonProperty("codprvext")
	private String codprvext;
	@JsonIgnore
	private boolean created;
	@JsonIgnore
	private boolean deleted;
	@JsonProperty("error")
	@ToString.Exclude
	private Error error;
}