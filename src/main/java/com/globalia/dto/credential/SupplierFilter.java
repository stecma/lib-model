package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class SupplierFilter implements Serializable {

	private static final long serialVersionUID = 4608771733410640699L;

	@JsonProperty("status")
	private String status;
	@JsonProperty("sys")
	private String system;
	@JsonProperty("sup")
	private String supplier;
	@JsonProperty("sub")
	private String subSupplier;
	@JsonProperty("tse")
	private String serviceType;
	@JsonProperty("own")
	private String ownProduct;
	@JsonProperty("othersrv")
	private String otherSrv;
	@JsonProperty("sap")
	private String sapCode;
	@JsonProperty("wbds")
	private String wbds;
	@JsonProperty("xsell")
	private String xsell;
	@JsonProperty("client")
	private String client;
	@JsonProperty("mce")
	private String buyModel;
	@JsonProperty("mlu")
	private String user;
	@JsonProperty("cpc")
	private String clientPref;
	@JsonProperty("giata")
	private String giata;
	@JsonProperty("mhr")
	private String multiHotel;
	@JsonProperty("timeout")
	private String timeout;
	@JsonProperty("regimen")
	private String generalRegimen;
	@JsonProperty("availgts")
	private String availCost;
	@JsonProperty("tcd")
	private String topDestination;
	@JsonProperty("sla")
	private String sla;
	@JsonProperty("filtergrp")
	private GroupFilter groupFilter;
	@JsonProperty("filterext")
	private ExternalFilter externalFilter;
}