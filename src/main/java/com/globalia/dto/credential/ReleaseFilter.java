package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class ReleaseFilter implements Serializable {

	private static final long serialVersionUID = -2989316647591402249L;

	@JsonProperty("brand")
	private String brand;
	@JsonProperty("tse")
	private String serviceType;
	@JsonProperty("cre")
	private String credential;
	@JsonProperty("pay")
	private String payType;
	@JsonProperty("days")
	private int days;
	@JsonProperty("ope")
	private String operationDays;
}