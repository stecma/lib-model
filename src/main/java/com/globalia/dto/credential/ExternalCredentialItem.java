package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.DateRange;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class ExternalCredentialItem extends BaseItemCredential {

	private static final long serialVersionUID = -5428106148393633846L;

	@JsonProperty("corp")
	private MasterItem corporation;
	@JsonProperty("user")
	private String user;
	@JsonProperty("pass")
	private String pass;
	@JsonProperty("checkIn")
	private DateRange checkIn;
	@JsonProperty("book")
	private DateRange book;
	@JsonProperty("invtype")
	private MasterItem invoicingType;
	@JsonProperty("connect")
	private String connector;
	@JsonProperty("new")
	private boolean create;
	@JsonProperty("supplierId")
	private String supplierId;
	@JsonProperty("groupId")
	private String groupId;
}