package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class GroupFilter implements Serializable {

	private static final long serialVersionUID = 6382460329121837261L;

	@JsonProperty("name")
	private String name;
	@JsonProperty("brand")
	private String brand;
	@JsonProperty("clientType")
	private String clientType;
	@JsonProperty("act")
	private String active;
	@JsonProperty("dft")
	private String byDefault;
	@JsonProperty("test")
	private String test;
	@JsonProperty("hotelx")
	private String hotelx;
	@JsonProperty("tagdiv")
	private String tagdiv;
	@JsonProperty("cur")
	private String currency;
	@JsonProperty("tagnac")
	private String tagnac;
	@JsonProperty("cou")
	private String country;
	@JsonProperty("model")
	private String distributionModel;
	@JsonProperty("rates")
	private String rates;
}