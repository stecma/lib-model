package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.enumeration.credential.HotelX;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class GroupCredentialItem extends BaseItemCredential {

	private static final long serialVersionUID = -1596866741636753492L;

	@JsonProperty("sysname")
	private String sysName;
	@JsonProperty("test")
	private boolean test;
	@JsonProperty("dft")
	private boolean byDefault;
	@JsonProperty("hotX")
	private HotelX hotelX;
	@JsonProperty("tagdiv")
	private boolean tagCurrency;
	@JsonProperty("tagnac")
	private boolean tagNationality;
	@JsonProperty("brand")
	private MasterItem brand;
	@JsonProperty("clientype")
	private MasterItem clientType;
	@JsonProperty("others")
	private String others;
	@JsonProperty("allMasters")
	private Set<String> allMasters;
	@JsonProperty("divs")
	private Set<MasterItem> currencies;
	@JsonProperty("cous")
	private Set<MasterItem> countries;
	@JsonProperty("exccous")
	private Set<MasterItem> excludedCountries;
	@JsonProperty("models")
	private Set<MasterItem> distriModels;
	@JsonProperty("rates")
	private Set<MasterItem> rates;
	@JsonProperty("configs")
	private Set<ExternalCredentialItem> externalConfig;
	@JsonProperty("new")
	private boolean create;
	@JsonProperty("assignment")
	private Assignment assignment;
	@JsonProperty("supplierId")
	private String supplierId;
}
