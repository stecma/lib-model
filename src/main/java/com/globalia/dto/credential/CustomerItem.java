package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.enumeration.credential.MatchLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class CustomerItem extends BaseItemCredential {

	private static final long serialVersionUID = -4625027429886646289L;

	@JsonProperty("credential")
	private String credential;
	@JsonProperty("assignment")
	private Assignment assignment;
	@JsonProperty("prior")
	private boolean priority;
	@JsonProperty("xsell")
	private boolean xsell;
	@JsonProperty("status")
	private MatchLevel status;
}
