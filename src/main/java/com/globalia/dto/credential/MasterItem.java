package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.BaseMasterItem;
import com.globalia.enumeration.credential.RateCategory;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class MasterItem extends BaseMasterItem {

	private static final long serialVersionUID = -8783065660127147912L;

	@JsonProperty("alias")
	private String alias;
	@JsonProperty("test")
	private Boolean test;
	@JsonProperty("demo")
	private Boolean demo;
	@JsonProperty("corp")
	private Boolean corporate;
	@JsonProperty("supplier")
	private Boolean showSupplier;
	@JsonProperty("supName")
	private String supplierName;
	@JsonProperty("subName")
	private String subSupplierName;
	@JsonProperty("rec")
	private String receptive;
	@JsonProperty("sap")
	private String sapCode;
	@JsonProperty("conpre")
	private String priceConcept;
	@JsonProperty("wbds")
	private String wbdsCode;
	@JsonProperty("own")
	private Boolean ownProduct;
	@JsonProperty("srvType")
	private String serviceType;
	@JsonProperty("srvOther")
	private String serviceOther;
	@JsonProperty("descSrvOther")
	private String descSrvOther;
	@JsonProperty("ratecat")
	private RateCategory rateCategory;
	@JsonProperty("filter")
	private String filter;
	@JsonProperty("addrem")
	private Boolean addRemarks;
	@JsonProperty("sendrem")
	private Boolean sendRemarks;
	@JsonProperty("shwrate")
	private Boolean showRates;
	@JsonProperty("numpas")
	private String numpas;
	@JsonProperty("supByZone")
	private Boolean supByZone;
	@JsonProperty("vcc")
	private Boolean vcc;
	@JsonProperty("rresp")
	private Boolean noEspecialReg;
	@JsonProperty("com")
	private Boolean combinable;
	@JsonProperty("hide")
	private Boolean hide;
	@JsonProperty("masters")
	private Set<MasterItem> masters;
}
