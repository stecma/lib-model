package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.globalia.dto.BaseAllItemsResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
public class AllItemsResponse extends BaseAllItemsResponse {

    private static final long serialVersionUID = -7644504430574868916L;

    private Set<CredentialItem> credentials;
    private Set<CustomerItem> customers;
    private Set<CancelCostItem> cancelCosts;
    private Set<ExternalCredentialItem> externals;
    private Set<GroupCredentialItem> groups;
    private Set<ReleaseItem> releases;
    private Set<SupplierItem> suppliers;
    private Set<SupplierByZoneItem> suppliersByZone;
    private Set<TradePolicyItem> tradePolicies;
    private Set<CurrencyItem> currencies;
    private Set<MasterItem> masters;
    private Set<CredentialCounterItem> credentialIndexCounters;
    private Set<CredentialCounterItem> credentialAliasCounters;
}
