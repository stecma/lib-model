package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.Parameter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode()
public class MatchItem implements Serializable {

	private static final long serialVersionUID = -392805705165468719L;

	@JsonProperty("cred")
	private String credentialId;
	@JsonProperty("sup")
	private String supplierId;
	@JsonProperty("grp")
	private String groupId;
	@JsonProperty("supplier")
	private SupplierItem supplier;
	@JsonProperty("customer")
	private CredentialItem customer;
	@JsonProperty("credentials")
	private Set<CredentialItem> credentials;
	@JsonProperty("suppliers")
	private Set<SupplierItem> suppliers;
	@JsonProperty("assign")
	private Set<Parameter> assigned;
	@JsonProperty("unassign")
	private Set<Parameter> unAssigned;
	@JsonProperty("othergrpagn")
	private Set<Parameter> otherGroupAssigned;
	// Assignment process
	@JsonProperty("codes")
	private Set<Parameter> codes;
	@JsonProperty("assignment")
	private Assignment assignment;
}