package com.globalia.dto.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.DateRange;
import com.globalia.enumeration.credential.PercentageType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.LinkedHashSet;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class TradePolicyItem extends BaseItemCredential {

	private static final long serialVersionUID = -4625027429886646289L;

	@JsonProperty("credential")
	private String credential;
	@JsonProperty("pct")
	private Double percentage;
	@JsonProperty("pcttype")
	private PercentageType percentageType;
	@JsonProperty("date")
	private DateRange date;
	@JsonProperty("dateApl")
	private DateRange dateApl;
	@JsonProperty("exccha")
	private boolean excChain;
	@JsonProperty("notes")
	private LinkedHashSet<Note> notes;
}
