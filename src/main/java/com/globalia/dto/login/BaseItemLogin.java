package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.BaseItem;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class BaseItemLogin extends BaseItem {

	private static final long serialVersionUID = -843672387210424673L;

	@JsonProperty("env")
	private String environment;
	@JsonProperty("icon")
	private String icon;
	@JsonProperty("users")
	private Set<String> users;
	@JsonProperty("masters")
	private Set<MasterItem> masters;
}
