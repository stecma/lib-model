package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.PairValue;
import com.globalia.exception.Error;
import com.globalia.exception.Warning;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserItem implements Serializable {

	private static final long serialVersionUID = 7246867655610095089L;

	@JsonProperty("user")
	private String id;
	@JsonProperty("pass")
	private String pass;
	@JsonProperty("adm")
	private boolean isAdmin;
	@JsonProperty("lang")
	private String language;
	@JsonProperty("name")
	private String name;
	@JsonProperty("sur")
	private String surname;
	@JsonProperty("mail")
	private String mail;
	@JsonProperty("act")
	private boolean isActive;
	@JsonProperty("photo")
	private String photo;
	@JsonProperty("bus")
	private LinkedHashSet<MasterItem> business;
	@JsonProperty("envs")
	private LinkedHashSet<MasterItem> environments;
	@JsonProperty("cias")
	private LinkedHashSet<MasterItem> companies;
	@JsonProperty("configMenu")
	private LinkedHashSet<UserMenu> configuration;
	@JsonProperty("codes")
	private LinkedHashSet<PairValue> codes;
	@JsonProperty("del")
	private boolean deleted;
	@JsonProperty("themes")
	private String themes;
	@JsonProperty("warnings")
	@ToString.Exclude
	private Set<Warning> warnings;
	@JsonProperty("error")
	@ToString.Exclude
	private Error error;
	@JsonIgnore
	private boolean created;
	private String token;
}
