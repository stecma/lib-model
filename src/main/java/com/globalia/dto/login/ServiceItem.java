package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class ServiceItem extends BaseItemLogin {

	private static final long serialVersionUID = -6014766039921934425L;

	@JsonProperty("url")
	private Url url;
	@JsonProperty("ext")
	private boolean isExternal;
	@JsonProperty("fie")
	private boolean forceIE;
}
