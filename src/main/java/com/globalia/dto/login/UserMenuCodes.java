package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserMenuCodes implements Serializable {

	private static final long serialVersionUID = -1744575851102397602L;

	@JsonProperty("menu")
	private String menu;
	@JsonProperty("grp")
	private String group;
	@JsonProperty("srv")
	private String service;
	@JsonProperty("read")
	private boolean readOnly;
}
