package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.Parameter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.LinkedHashSet;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Url implements Serializable {

	private static final long serialVersionUID = -3778086372662193739L;

	@JsonProperty("url")
	private String route;
	@JsonProperty("params")
	private LinkedHashSet<Parameter> params;
	@JsonProperty("readonly")
	private boolean readOnly;
}