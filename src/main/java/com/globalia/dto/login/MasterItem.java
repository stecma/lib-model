package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.dto.BaseMasterItem;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class MasterItem extends BaseMasterItem {

	private static final long serialVersionUID = -8783065660127147912L;

	@JsonProperty("host")
	private String host;
	@JsonProperty("logo")
	private String logo;
	@JsonProperty("masters")
	private Set<MasterItem> masters;
}
