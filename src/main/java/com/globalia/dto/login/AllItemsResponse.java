package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.globalia.dto.BaseAllItemsResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.LinkedHashSet;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
public class AllItemsResponse extends BaseAllItemsResponse {

	private static final long serialVersionUID = -7644504430574868916L;

	private LinkedHashSet<UserItem> users;
	private LinkedHashSet<MenuItem> menus;
	private LinkedHashSet<GroupItem> groups;
	private LinkedHashSet<ServiceItem> services;
	private LinkedHashSet<MasterItem> masters;
}
