package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.exception.Warning;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class UserMenu implements Serializable {

	private static final long serialVersionUID = -1744575851102397602L;

	@JsonProperty("env")
	private String environment;
	@JsonProperty("cia")
	private String company;
	@JsonProperty("menus")
	private LinkedHashSet<MenuItem> menus;
	@JsonProperty("codes")
	private LinkedHashSet<UserMenuCodes> codes;
	@JsonProperty("warnings")
	@ToString.Exclude
	private Set<Warning> warnings;
}
