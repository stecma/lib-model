package com.globalia.dto.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.globalia.dto.BaseItemResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
public class ItemResponse extends BaseItemResponse {

	private static final long serialVersionUID = -7644504430574868916L;

	private UserItem user;
	private MenuItem menu;
	private GroupItem group;
	private ServiceItem service;
	private MasterItem master;
}
