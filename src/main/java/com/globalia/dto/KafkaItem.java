package com.globalia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class KafkaItem implements Serializable {

	private static final long serialVersionUID = 7122751821068068595L;

	@JsonProperty("key")
	private String key;
	@JsonProperty("entity")
	private String entity;
	@JsonProperty("action")
	private String action;
	@JsonProperty("json")
	private String json;
}
