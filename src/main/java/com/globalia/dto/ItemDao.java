package com.globalia.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ItemDao implements Serializable {

	private static final long serialVersionUID = -4874772528592226510L;

	@Getter
	@Setter
	private String id;
	@Getter
	@Setter
	private String value;
	@Getter
	@Setter
	private String updateDate;
}