package com.globalia.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.LinkedHashSet;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Item implements Serializable {

	private static final long serialVersionUID = 7556631170553958383L;

	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private LinkedHashSet<I18n> names;
	@JsonProperty("desc")
	private LinkedHashSet<I18n> descriptions;
}
