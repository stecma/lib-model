package com.globalia.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Set;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class BaseMasterItem extends BaseItem {

	private static final long serialVersionUID = -8783065660127147912L;

	@JsonProperty("entity")
	private String entity;
	@JsonProperty("allMasters")
	private Set<String> allMasters;
	@JsonProperty("params")
	private Set<Parameter> params;
	@JsonIgnore
	private boolean created;
}
