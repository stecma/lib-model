package com.globalia.dao;

import com.globalia.dto.BaseItemResponse;

public interface ISave<T> {

	/**
	 * Create Item.
	 *
	 * @param item New Object.
	 * @return T Object.
	 */
	BaseItemResponse createItem(final T item);

	/**
	 * Update item.
	 *
	 * @param item Object.
	 * @return T Object.
	 */
	BaseItemResponse updateItem(final T item);

	/**
	 * Delete item.
	 *
	 * @param item Object.
	 * @return T Object.
	 */
	BaseItemResponse deleteItem(final T item);
}
