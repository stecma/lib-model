package com.globalia.dao;

import com.globalia.dto.BaseAllItemsResponse;
import com.globalia.dto.BaseItemResponse;

public interface IFind<T> {

	/**
	 * Get item.
	 *
	 * @param item Object.
	 * @return T Object
	 */
	BaseItemResponse getItem(final T item);

	/**
	 * Get item list.
	 *
	 * @param item Object.
	 * @return U Object.
	 */
	BaseAllItemsResponse getAllItems(final T item);
}
