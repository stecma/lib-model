package com.globalia.enumeration.credential;

public enum RateCategory {
	CUSTOMER,
	SUPPLIER,
	BOTH
}
