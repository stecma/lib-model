package com.globalia.enumeration.credential;

public enum MatchLevel {
	GREEN,
	YELLOW,
	RED
}
