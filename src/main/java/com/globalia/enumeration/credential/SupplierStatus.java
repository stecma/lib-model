package com.globalia.enumeration.credential;

public enum SupplierStatus {
	ENABLED,
	DISABLED,
	PARTIALLY
}
