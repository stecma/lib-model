package com.globalia.enumeration.credential;

public enum PercentageType {
	MARKUP,
	COMMISSION
}
