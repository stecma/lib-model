package com.globalia.enumeration;

/**
 * Defined object to know which layer causes the error.
 */
public enum ErrorLayer {
	PERSIST_LAYER(100, "JPA Layer"),
	AUTH_LAYER(101, "Auth Layer"),
	SERVICE_LAYER(102, "Service Layer"),
	REPOSITORY_LAYER(103, "Repository Layer"),
	PROXY_LAYER(104, "Proxy Layer"),
	API_LAYER(106, "API Layer"),
	UNKNOWN_LAYER(200, "Unknown Layer");

	private final int layerCode;
	private final String reason;

	ErrorLayer(final int value, final String reasonPhrase) {
		this.layerCode = value;
		this.reason = reasonPhrase;
	}

	public static ErrorLayer valueOf(final int layerCode) {
		ErrorLayer status = ErrorLayer.resolve(layerCode);
		if (status == null) {
			throw new IllegalArgumentException("No matching constant for [" + layerCode + "]");
		} else {
			return status;
		}
	}

	public static ErrorLayer resolve(final int layerCode) {
		ErrorLayer[] var1 = values();
		ErrorLayer type = null;
		for (ErrorLayer status : var1) {
			if (status.layerCode == layerCode) {
				type = status;
				break;
			}
		}
		return type;
	}

	@Override
	public String toString() {
		return this.layerCode + " " + this.reason + " " + this.name();
	}
}
