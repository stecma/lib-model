package com.globalia.enumeration.login;

public enum ActionProfile {
	CREATE,
	UPDATE,
	DELETE,
	PROCESS
}
