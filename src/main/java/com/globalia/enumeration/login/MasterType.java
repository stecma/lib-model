package com.globalia.enumeration.login;

public enum MasterType {
	COMPANY,
	ENVIRONMENT,
	LANGUAGES_APP,
	BUSINESS_APP,
}
