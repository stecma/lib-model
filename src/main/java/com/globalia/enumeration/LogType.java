package com.globalia.enumeration;

public enum LogType {
	NONE,
	ERROR,
	WARN,
	INFO,
	DEBUG
}
