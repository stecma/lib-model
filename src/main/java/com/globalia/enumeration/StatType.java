package com.globalia.enumeration;

public enum StatType {
	/**
	 * Init request.
	 */
	INIT,
	/**
	 * JSON to Object.
	 */
	UNMARSHALL,
	/**
	 * Object to JSON.
	 */
	MARSHALL,
	/**
	 * Encrypt.
	 */
	ENCRYPT,
	/**
	 * Decrypt.
	 */
	DECRYPT,
	/**
	 * Select.
	 */
	SQL_QUERY,
	/**
	 * Insert.
	 */
	SQL_INSERT,
	/**
	 * Update.
	 */
	SQL_UPDATE,
	/**
	 * Delete.
	 */
	SQL_DELETE,
	/**
	 * Get values from redis.
	 */
	REDIS_GET,
	/**
	 * Add or update values from redis.
	 */
	REDIS_ADD,
	/**
	 * Delete values from redis.
	 */
	REDIS_DEL
}
