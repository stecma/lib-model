package com.globalia.redis;

import com.globalia.dto.BaseItemResponse;
import com.globalia.monitoring.Monitor;

public interface ISave<T> {

	/**
	 * Add Item.
	 *
	 * @param item    New Object.
	 * @param monitor Monitor object.
	 * @return T Object.
	 */
	BaseItemResponse addItem(final T item, final Monitor monitor);
}
