package com.globalia.redis;

import com.globalia.dto.BaseAllItemsResponse;
import com.globalia.monitoring.Monitor;

public interface IFindAll<T> {

	/**
	 * Get item list.
	 *
	 * @param item    Object.
	 * @param monitor Monitor object.
	 * @return U Object.
	 */
	BaseAllItemsResponse getAllItems(final T item, final Monitor monitor);
}
