package com.globalia.service;

import com.globalia.dto.BaseAllItemsResponse;
import com.globalia.monitoring.Monitor;

public interface IFindAll<T> {

	/**
	 * Get item list.
	 *
	 * @param item Object.
	 * @return U Object.
	 */
	BaseAllItemsResponse getAllItems(final T item, final Monitor monitor);
}
