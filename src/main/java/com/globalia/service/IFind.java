package com.globalia.service;

import com.globalia.dto.BaseItemResponse;
import com.globalia.monitoring.Monitor;

public interface IFind<T> {

	/**
	 * Get item.
	 *
	 * @param item Object.
	 * @return T Object
	 */
	BaseItemResponse getItem(final T item, final Monitor monitor);
}
