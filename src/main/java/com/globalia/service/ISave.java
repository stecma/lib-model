package com.globalia.service;

import com.globalia.dto.BaseItemResponse;
import com.globalia.monitoring.Monitor;

public interface ISave<T> {

	/**
	 * Create Item.
	 *
	 * @param item    New Object.
	 * @param monitor Monitor object.
	 * @return T Object.
	 */
	BaseItemResponse createItem(final T item, final Monitor monitor);

	/**
	 * Update item.
	 *
	 * @param item    Object.
	 * @param monitor Monitor object.
	 * @return T Object.
	 */
	BaseItemResponse updateItem(final T item, final Monitor monitor);

	/**
	 * Delete item.
	 *
	 * @param item    Object.
	 * @param monitor Monitor object.
	 * @return T Object.
	 */
	BaseItemResponse deleteItem(final T item, final Monitor monitor);
}
