package com.globalia.api.credential;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.api.BaseApiRQ;
import com.globalia.dto.credential.CancelCostItem;
import com.globalia.dto.credential.CredentialItem;
import com.globalia.dto.credential.CurrencyItem;
import com.globalia.dto.credential.CustomerItem;
import com.globalia.dto.credential.MassiveItem;
import com.globalia.dto.credential.MasterItem;
import com.globalia.dto.credential.MatchItem;
import com.globalia.dto.credential.ReleaseItem;
import com.globalia.dto.credential.SupplierByZoneItem;
import com.globalia.dto.credential.SupplierItem;
import com.globalia.dto.credential.TradePolicyItem;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Object defined for the API request.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApiRQ extends BaseApiRQ {

	private static final long serialVersionUID = -2338903960647258920L;

	@JsonProperty("credential")
	private CredentialItem credential;
	@JsonProperty("supplier")
	private SupplierItem supplier;
	@JsonProperty("supplierbyzone")
	private SupplierByZoneItem supplierbyzone;
	@JsonProperty("match")
	private MatchItem match;
	@JsonProperty("trade")
	private TradePolicyItem trade;
	@JsonProperty("massive")
	private MassiveItem massive;
	@JsonProperty("custom")
	private CustomerItem custom;
	@JsonProperty("release")
	private ReleaseItem release;
	@JsonProperty("cancelcost")
	private CancelCostItem cancelCost;
	@JsonProperty("currency")
	private CurrencyItem currency;
	@JsonProperty("master")
	private MasterItem master;
}
