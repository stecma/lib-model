package com.globalia.api.login;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.api.BaseApiRQ;
import com.globalia.dto.login.GroupItem;
import com.globalia.dto.login.LoginItem;
import com.globalia.dto.login.MasterItem;
import com.globalia.dto.login.MenuItem;
import com.globalia.dto.login.ServiceItem;
import com.globalia.dto.login.UserItem;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Object defined for the API request.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ApiRQ extends BaseApiRQ {

	private static final long serialVersionUID = 3229336432402420710L;

	@JsonProperty("login")
	private LoginItem login;
	@JsonProperty("service")
	private ServiceItem service;
	@JsonProperty("group")
	private GroupItem group;
	@JsonProperty("menu")
	private MenuItem menu;
	@JsonProperty("user")
	private UserItem user;
	@JsonProperty("master")
	private MasterItem master;
}
