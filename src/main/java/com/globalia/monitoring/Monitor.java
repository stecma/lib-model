package com.globalia.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Monitor implements Serializable {

	private static final long serialVersionUID = 2592408028214150516L;

	@JsonProperty("times")
	private long timestamp;
	@JsonProperty("end")
	private long endDate;
	@JsonProperty("uid")
	private String correlationID;
	@JsonProperty("route")
	private String route;
	@JsonProperty("logs")
	private Set<Log> logs;
	@JsonProperty("stats")
	private Set<Stat> stats;
	@JsonIgnore
	private String service;
	@JsonIgnore
	private String port;
}
