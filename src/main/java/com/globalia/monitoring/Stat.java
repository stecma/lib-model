package com.globalia.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.enumeration.StatType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@EqualsAndHashCode
public class Stat implements Serializable {

	private static final long serialVersionUID = -644256525704965858L;

	@JsonProperty("times")
	private long timestamp;
	@JsonProperty("app")
	private String service;
	@JsonProperty("type")
	private StatType type;
	@JsonProperty("eld")
	private long elapsedMS;
	@JsonProperty("host")
	private String host;
	@JsonProperty("port")
	private String port;
}