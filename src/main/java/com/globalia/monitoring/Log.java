package com.globalia.monitoring;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.globalia.enumeration.LogType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@EqualsAndHashCode
public class Log implements Serializable {

	private static final long serialVersionUID = -6420931160640277082L;

	@JsonProperty("times")
	private long timestamp;
	@JsonProperty("type")
	private LogType type;
	@JsonProperty("app")
	private String service;
	@JsonProperty("host")
	private String host;
	@JsonProperty("port")
	private String port;
	@JsonProperty("cls")
	private String clazz;
	@JsonProperty("mtd")
	private String method;
	@JsonProperty("line")
	private int line;
	@JsonProperty("msg")
	private String message;
}
