package com.globalia.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.globalia.enumeration.ErrorLayer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * Object defined for internal error control.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Warning implements Serializable {

	private static final long serialVersionUID = -5412668021650873270L;

	private String message;
	private ErrorLayer errorLayer;
	private Date timestamp;

	public Warning(final String msgText, final ErrorLayer layer) {
		this.message = msgText;
		this.errorLayer = layer;
		this.timestamp = new Date();
	}
}
