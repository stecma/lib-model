package com.globalia.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.globalia.monitoring.Monitor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Object defined for the exception control.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode(callSuper = true)
public class ValidateException extends RuntimeException {

	@Getter
	private Error error;
	@Getter
	private Monitor monitor;

	public ValidateException(final Error error, final Monitor metrics) {
		super();
		this.error = error;
		this.monitor = metrics;
	}
}