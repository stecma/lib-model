package com.globalia.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.globalia.enumeration.ErrorLayer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Object defined for internal error control.
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Error implements Serializable {

	private static final long serialVersionUID = -5412668021650873270L;

	private String uri;
	private int status;
	private Set<String> messages;
	private ErrorLayer errorLayer;
	private Date timestamp;

	public Error(final String msgText, final ErrorLayer layer, final int status) {
		this(msgText, layer);
		this.status = status;
	}

	public Error(final String msgText, final ErrorLayer layer) {
		this.messages = new HashSet<>();
		this.messages.add(msgText);
		this.errorLayer = layer;
		this.timestamp = new Date();
	}
}
